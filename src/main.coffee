#router = require "./router.coffee"
Address = require "./address.coffee"
scroll = null
block = false

app.loaded =
  page: false
  map: false

class customControl
  constructor: (@map) ->

    @element = document.createElement "div"
    @element.style.padding = "0 33px 33px 0"
    @element.index = 1

    @inner = document.createElement("div")
    @inner.style["font-size"] = "41px"
    @inner.style["line-height"] = "39px"
    @inner.style["text-align"] = "center"
    @inner.style.cursor = "pointer"
    @inner.style.height = "40px"
    @inner.style.width = "40px"
    @inner.style.color = "#FFF"

    @inner.innerHTML = "&nbsp;"

    @element.appendChild @inner

    self = @

    google.maps.event.addDomListener @element, "click", ->
      self.click self.map


  click: ->
    console.log "Zoom"

class zoomIn extends customControl
  constructor: (@map) ->
    super @map
    @element.style.padding = "33px 33px 2px 0"
    @inner.innerHTML = "<img src='/img/zin.jpg'>"

  click: (@map) ->
    console.log @map.getZoom() - 1
    @map.setZoom @map.getZoom() + 1

class zoomOut extends customControl
  constructor: (@map) ->
    super @map
    @inner.innerHTML = "<img src='/img/zout.jpg'>"

  click: (@map) ->
    console.log @map.getZoom() - 1
    @map.setZoom @map.getZoom() - 1


app.checkLoaded = (item) ->
  console.log item
  app.loaded[item] = true
  console.log app.loaded

  valid = true

  for i, val of app.loaded
    valid = false unless val



  if valid
    $(".header").removeClass("rev")
    setTimeout ->
      $(".header").addClass("rev")
    , 500
    $("body").removeClass("unloaded")

    calc()

$(window).on "resize", ->
  calc()

calc = ->
  console.log "calc"
  app.trigger "calc"

window.onload = ->
  root = app {}
  root.on "ready", calc


app (data) ->

  app.checkLoaded "page"

  $("form").on "submit", ->
    form = @
    data = $(form).formParams()
    re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if re.test(data.email) && data.agree? && data.agree is "on"

      ga "send", "event", "click", (if data.type is "product" then "main-email" else "prof-email")

      $.post "/form", data
      .done (res) ->
          $(form).find("input[type='text']").val("")
          popup = $("<div></div>", {class: "popup", html: "<div class='close'></div><div class='popup-text'><h4>Спасибо за подписку на наши новости.</h4>Мы выслали тебе письмо с дополнительной информацией.</div>"})
          popup_p = $(form).parents(".tocenter").parent()
          p = (popup_p.height() - 160) / 2
          popup.css
            "top": p

          popup_p.css("position", "relative").append(popup)
          popup.find(".close").one "click", ->
            popup.fadeOut()



    else

      $(form).find("input[type='text']").addClass "error"
      .focus()
      .one "change", ->
          $(this).removeClass "error"
      .one "keyup", ->
          $(this).removeClass "error"

    return false



  switch location.pathname
    when "/" then $("#menu li.main").addClass "active"
    when "/professional" then $("#menu li.professional").addClass "active"
    when "/products" then $("#menu li.products").addClass "active"



  $(window).scrollTop(0)
  $.smoothScroll
    scrollTarget: "." + document.pages[0]

  calc()

  $("#menu").sticky({topSpacing:0});

  $(".header a.button").on "click", ->
    $(window).scrollTop(1)
    return false


  if $("#map").length

    styleOpts = [
      {
        "featureType": "landscape",
        "stylers": [
          { "saturation": -100 }
        ]
      },{
        "featureType": "administrative",
        "stylers": [
          { "saturation": -100 }
        ]
      },{
        "featureType": "poi",
        "stylers": [
          { "saturation": -100 }
        ]
      },{
        "featureType": "road",
        "stylers": [
          { "saturation": -100 }
        ]
      },{
        "featureType": "water",
        "stylers": [
          { "saturation": -100 }
        ]
      },{
        "featureType": "water"  }
    ]

    map = new google.maps.Map document.getElementById("map"),
      zoom: 12
      center: {lat: 55.7428645, lng: 37.6523432}
      disableDefaultUI: true

    map.setOptions
      styles: styleOpts
      scrollwheel: false
#    map.disableScrollWheelZoom();

    zout = new zoomOut map
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(zout.element)

    zin = new zoomIn map
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(zin.element)


    geocoder = new google.maps.Geocoder

    $.get '/shops'
    .done (res) ->
        app.checkLoaded "map"
        console.log res

        count = res.length
        checked = 0
        bounds = new google.maps.LatLngBounds

        check = ->
          ++checked
          if checked is count
            map.fitBounds(bounds)

        infoWin = null
        setTimeout ->
          for item in res
            (new Address item, bounds, geocoder, map)
            .on "loaded", ->
              check()
            .on "click", (win)->
                infoWin.close() if infoWin
                infoWin = win
        , 1000

  else app.checkLoaded "map"