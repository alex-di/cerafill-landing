app (data) ->
  app.on "calc", ->

    app.loaded.video = false
    v = videojs "video",
      autoplay: true
      loop: true
      width: 1280
      height: 900
      muted: true
    , ->
      app.video = this
    v.on "loadeddata", ->
      app.checkLoaded "video"

    v.on "timeupdate", (some) ->
      if some.currentTarget? && some.currentTarget.currentTime is 0
        if $(".header").hasClass "rev"
          $(".header").removeClass "rev"
          setTimeout ->
            $(".header").addClass "rev"
          , 100

    h = $(window).height() - 134
    w = $(window).width()
    p = w / h

    stand = 1920 / 1080


    if p < stand
      app.video.height h + 10
      app.video.width (h + 10) * stand

    else
      app.video.width w
      app.video.height w / stand

      $(".video").height h



