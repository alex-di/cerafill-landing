gulp = require "gulp"
browserify = require "gulp-browserify"
watch = require "gulp-watch"
plumber = require "gulp-plumber"
coffee = require 'gulp-coffee'
rename = require 'gulp-rename'

gulp.task "default", ->
  watch "src/*.coffee", ->
    gulp.src "src/main.coffee", { read: false }
    .pipe plumber()
        .pipe browserify
            insertGoals: true
            debug: true
            transform: ["coffeeify"]
            extensions: ['.coffee']

          .pipe rename "main.js"
          .pipe gulp.dest "./public/js"


    gulp.src "src/scroll.coffee", {read: false}
    .pipe plumber()
      .pipe browserify
          insertGoals: true
          debug: true
          transform: ["coffeeify"]
          extensions: ['.coffee']
        .pipe rename "scroll.js"
          .pipe gulp.dest "./public/js"



    gulp.src "src/video.coffee", {read: false}
    .pipe plumber()
      .pipe browserify
          insertGoals: true
          debug: true
          transform: ["coffeeify"]
          extensions: ['.coffee']
        .pipe rename "video.js"
          .pipe gulp.dest "./public/js"



    gulp.src "src/slider.coffee", {read: false}
    .pipe plumber()
      .pipe browserify
          insertGoals: true
          debug: true
          transform: ["coffeeify"]
          extensions: ['.coffee']
        .pipe rename "slider.js"
          .pipe gulp.dest "./public/js"

    gulp.src "src/fixed.coffee", {read: false}
    .pipe plumber()
      .pipe browserify
          insertGoals: true
          debug: true
          transform: ["coffeeify"]
          extensions: ['.coffee']
        .pipe rename "fixed.js"
          .pipe gulp.dest "./public/js"


#  watch {glob: "public-source/css/*.css"},  ->
#    gulp.src "public-source/css/*.css"
#    .pipe plumber()
#      .pipe browserify
#          insertGoals: true
#          debug: true
