http = require "http"
mongoose = require "mongoose"
config = require "./config"
mongoose.connect config.db
express = require "express"
moment = require "moment"
path = require "path"
bp = require "body-parser"
email = require 'emailjs'
jade = require "jade"

app = express()
app.set 'port', process.env.PORT || 4055
app.set('trust proxy', true);
app.set('view engine', 'jade')
.use express.static path.join path.dirname(require.main.filename), './public'

app.use(bp.urlencoded({ extended: false }))

app.use(bp.json())
device = require "express-device"

app.use device.capture()
device.enableDeviceHelpers app

Shop = require "./models/shop"
Email = require "./models/email"

cp = require "child_process"

#checkGeo = ->
#  cp.fork "./geocoder.coffee"

#checkGeo()

app.set('views', path.join(__dirname, 'views'));

app.get "/", (req, res) ->
  res.render "index"
#  checkGeo()

app.get "/shops", (req, res) ->
  Shop.find {}, (err, result) ->
    res.json result

app.post "/shops/:id", (req, res) ->
  Shop.findById req.params.id, (err, result) ->
    console.log result, req.body
    d = JSON.parse req.body.position
    result.position =
      lat: d.k
      lng: d.B

    result.save()
    res.send(200).end()

app.get "/professional", (req, res) ->
  res.render "professional"

app.get "/products", (req, res) ->
  res.render "products"

app.post "/form", (req, res) ->

  res.send 200
  item = new Email req.body
  item.save()


  server = email.server.connect
    user: "info@cerafill.ru"
    password: "Cerafill911"
    host: "smtp.yandex.ru"
    ssl: true
    port: 465


  text = jade.renderFile path.join path.dirname(require.main.filename), "./views/mail/" + (if item.type is "product" then "product.jade" else "professional.jade")
  server.send
      text: text,
      from: "Cerafill <info@cerafill.ru>"
      to: item.email
      subject: (if item.type is "product" then "Получи консультацию о состоянии волос" else  "Семинары для стилистов")
      attachment: [
        data: text
        alternative: true
      ]
    ,
    (err, mess) ->
      if err
        console.log err
#        throw err
      console.log mess

server = http.createServer app
.listen app.get 'port'
