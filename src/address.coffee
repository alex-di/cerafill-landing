module.exports = (item, bounds, geocoder, map) ->
  self = riot.observable @

  self.d = item

  self.putMarker = ->
    pos = if self.d.position.lat? && typeof self.d.position.lat != "function" then new google.maps.LatLng(self.d.position.lat,self.d.position.lng) else self.d.position

    bounds.extend(pos);

    marker = new RichMarker
      position: pos
      map: map
      content: "<div class='map-marker'></div>"
      flat: true

    info = new google.maps.InfoWindow
      pixelOffset: new google.maps.Size 0, -30
      content: "<div><b>" + self.d.name + "</b><br><span style='white-space: nowrap'>" + self.d.address.replace(" ,", ",") + "</span></div>"

    google.maps.event.addListener marker, 'click', ->
      self.trigger "click", info
      info.open(map, marker)

    self.trigger "loaded"

  self.addP = () ->
    $.ajax {
      url: "/shops/" + self.d._id
      type: "POST"
      data: {position: JSON.stringify(self.d.position)}
    }

  unless self.d.position?
    geocoder.geocode
      address: self.d.address
    , (results, status) ->
      if results? && results[0]?
        self.d.position = results[0].geometry.location

        self.putMarker()
        self.addP()

  else
    self.putMarker()

