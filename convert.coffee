xls = require "xlsx"
fs = require "fs"
mongoose = require "mongoose"
config = require "./config"
mongoose.connect config.db

Shop = require "./models/shop"

wb = xls.readFile "./addresses.xlsx"
data = xls.utils.sheet_to_json wb.Sheets[wb.Workbook.Sheets[0].name]


console.log data.length
for shop, i in data
  console.log i
  addr = []
  addr.push shop.sity if shop.sity
  addr.push shop.street if shop.street
  addr.push shop.house if shop.house
  addr.push shop.bld if shop.bld

  (new Shop
    address: addr.join ", "
    name: shop.name
    phone: shop.phone
  ).save()