mongoose = require "mongoose"

Email = new mongoose.Schema
  date:
    default: Date.now
    type: Date
  email: String
  type: String

module.exports = mongoose.model "Email", Email