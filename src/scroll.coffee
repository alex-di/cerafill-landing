app (data) ->
  scroll = app.scroll
  scrollController = new ScrollMagic();
  pages = document.pages
  block = false
  console.log "scroll l"

  app.on "calc", ->
    console.log "scroll calc"
    pages = document.pages

    h = $(window).height() - 134
    w = $(window).width()
    p = w / h
    $("section.full").height(h)


    #  $(".reasons").height($(window).height())

    if h > 550
      $(".margin-control").css("margin-bottom", (h - 550) / 6)

    $("#map").height h - 320

    $(".tocenter").each ->
      c = $(@)
      pr = $(@).parent().height()
      m = (pr - c.height()) / 2


      c.css
        "padding-top": m
        "padding-bottom": m



    keys = [37, 38, 39, 40];

    preventDefault = (e) ->
      e = e || window.event;
      if (e.preventDefault)
        e.preventDefault();
      e.returnValue = false;

    keydown = (e) ->
      for i in keys.length
        if e.keyCode is i
          preventDefault(e)
          return;

    wheel = (e) ->
      preventDefault(e)

    disable_scroll = ->
      if window.addEventListener
        window.addEventListener('DOMMouseScroll', wheel, false)

      window.onmousewheel = document.onmousewheel = wheel;
      document.onkeydown = keydown;

    enable_scroll = ->
      if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', wheel, false);
        window.onmousewheel = document.onmousewheel = document.onkeydown = null;

    scroll.destroy() if scroll?
    scroll = new ScrollScene
      triggerElement: "#scroller",
      duration: 1500
    .addTo scrollController
      .on "update", (e) ->
          unless block
            block = true
            disable_scroll()

            index = pages.indexOf $(document.querySelectorAll("section:hover")).data("section")
            index = 0 if index < 0

            #          console.log "index", $(document.querySelectorAll("section:hover")).data("section"), index
            direction = if e.target.parent().info("scrollDirection") is "FORWARD" then 1 else if e.target.parent().info("scrollDirection") is "REVERSE" then -1 else 0
            #          console.log "dir", direction

            if pages[index + direction]?
              #            console.log "." + pages[index + direction]
              $.smoothScroll
                scrollTarget: "." + pages[index + direction]

            setTimeout ->
              block = false
              enable_scroll()
            , 1000
